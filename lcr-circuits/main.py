import numpy as np
import plotly.graph_objects as go



def main():
    R = 10.0
    L = 500.0
    C = 11.8
    
    l = R / (2 * L)
    print("l: ", l)
    omega_0 = np.sqrt(1.0 / (L * C)) 
    print("omega_0: ", omega_0)
    omega = np.sqrt(omega_0 ** 2 - l ** 2)
    delta = 0
    A = 4.4
    q = lambda t : A * np.exp(-l * t) * np.cos(omega * t + delta)
    
    t = np.linspace(0.0, 2000, 4000)
    traces = [go.Scatter(x=t, y=q(t), name="Q of lcr-circut")]
    
    go.Figure(traces)

if __name__ == "__main__":
    main()