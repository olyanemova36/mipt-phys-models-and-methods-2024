EPS_0 = 8.85e-12

class Capacitor(CircutCompenent): 
    def __init__(self, 
                 name: str, 
                 eps: float,            # The dielectric constant of environment
                 S: float,              # Square
                 d: float):             # Distance
        CircutCompenent.__init__(name)
        self.eps = eps
        self.S = S
        self.d = d
        self.C = (eps * EPS_0 * S) / d  # Capacity
        
        